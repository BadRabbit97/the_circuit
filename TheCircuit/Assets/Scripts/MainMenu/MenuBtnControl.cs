﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBtnControl : MonoBehaviour
{
    //*****MENU BUTTON CONTROL*****
    //This script will control the onscreen selcetion of buttons

    //*****VARIABLES*****
    //Stores audio file
    public AudioSource audioSource;

    //Integer variable (numeric), stores what index the user is on
    public int index;

    //Boolean variable (TrueFalse), SerializeField = seen but not editable in Editor, stores if button is pressed
    [SerializeField] public bool keyDown;
    //Integer variable (numeric), SerializeField = seen but not editable in Editor, stores how many index there are
    [SerializeField] public int maxIndex;

    //*****UPDATES******
    // Start is called before the first frame update
    void Start()
    {
        //Gets the audio file
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Check to see if user is pressing up or down
        //If button is pressed...
        if (Input.GetAxis ("Vertical") != 0)
        {
            //Stops user spamming the menu
            if (!keyDown)
            {
                //If the user presses down...
                if (Input.GetAxis ("Vertical") < 0)
                {
                    //If the max index is not met...
                    if (index < maxIndex)
                    {
                        //...increase index
                        index++;
                    } 
                    //If the max index is met - bottom of menu...
                    else
                    {
                        //...index will loop back to the top
                        index = 0;
                    }
                } 
                //If the user presses up...
                else if (Input.GetAxis ("Vertical") > 0)
                {
                    //If the index is greater than 0...
                    if (index > 0)
                    {
                        //...decrease the index
                        index--;
                    }
                    //If the index is less than 0 - top of menu...
                    else
                    {
                        //...index will loop to the bottom of menu
                        index = maxIndex;
                    }
                }
                //Button selection can move
                keyDown = true;
            }
        }
        //If button is not pressed...
        else
        {
            //...nothing moves
            keyDown = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimFunction : MonoBehaviour
{
    //*****MENU ANIMATOR FUNCTION*****
    //This script plays the audio for the menu

    //*****VARIABLE*****
    //Referance MenuBtnControl script, SerializeField = seen but not editable in Editor
    [SerializeField] public MenuBtnControl menuBtnController;

    //Boolean variable (TrueFalse), stores if audio is activated
    public bool disableOnce;

    //*****FUNCTIONS*****
    //Controlls audio sound in menu
    void PlaySound(AudioClip whichSound)
    {
        //If button anim begins...
        if (!disableOnce)
        {
            //...tells audio source to play sound once
            menuBtnController.audioSource.PlayOneShot(whichSound);
        }
        //If no button anim begins...
        else
        {
            //...no audio is played
            disableOnce = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBtn : MonoBehaviour
{
    //*****MENU BUTTON*****
    //This script selects booleans for the animator and tells the state machine what to play

    //*****VARIABLES*****
    //Referance MenuBtnControl script, SerializeField = seen but not editable in Editor
    [SerializeField] public MenuBtnControl menuBtnController;
    //Referance Animator classes, SerializeField = seen but not editable in Editor
    [SerializeField] public Animator animator;
    //Referance MenuAnimFunction script, SerializeField = seen but not editable in Editor
    [SerializeField] public MenuAnimFunction menuAnimFunction;

    //Integer variable (numeric), SerializeField = seen but not editable in Editor, stores index value of btn
    [SerializeField] public int thisIndex;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //If MenuBtnControl index is the same as btn index...
        if (menuBtnController.index == thisIndex)
        {
            //Sets selected animator to true - activates it
            animator.SetBool("selected", true);
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //...sets pressed animator to true - activates it
                animator.SetBool("pressed", true);
            }
            //If btn is released...
            else if (animator.GetBool("pressed"))
            {
                //...sets pressed animator back to false - deactivated it
                animator.SetBool("pressed", false);
                //Activates MenuAnimFunction script
                menuAnimFunction.disableOnce = true;
            }
        }
        //If MenuBtnControl index is NOT the same as btn index...
        else
        {
            //...sets selected animator to false - deactivated it
            animator.SetBool("selected", false);
        }
    }
}

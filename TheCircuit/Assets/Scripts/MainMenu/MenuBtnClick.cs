﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBtnClick : MonoBehaviour
{
    //*****MENU BUTTON CLICK*****
    //This script controls where each button takes the user

    //*****VARIABLES*****
    //Referance MenuBtnControl script, SerializeField = seen but not editable in Editor
    [SerializeField] public MenuBtnControl menuBtnController;

    //Stores audio file
    public AudioSource btnClick;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //When page loads time scale always equals 1
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //If index equal 0 - play button...
        if (menuBtnController.index == 0)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays audio file
                btnClick.Play();

                //Call PlayGame function...
                StartCoroutine(PlayGame());
            }
        }

        //If index equal 1 - high score button...
        if (menuBtnController.index == 1)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays audio file
                btnClick.Play();

                //Call HighScore function...
                StartCoroutine(HighScore());
            }
        }

        //If index equal 2 - credit button...
        if (menuBtnController.index == 2)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays audio file
                btnClick.Play();

                //Call QuitGame function...
                StartCoroutine(CreditPage());
            }
        }

        //If index equal 3 - quit button...
        if (menuBtnController.index == 3)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays audio file
                btnClick.Play();

                //Call QuitGame function...
                StartCoroutine(QuitGame());
            }
        }
    }

    //*****FUNCTIONS*****
    //Play button
    IEnumerator PlayGame()
    {
        //Waits so many secounds before starting the next action
        yield return new WaitForSeconds(0.6f);
        //Loads the game level
        SceneManager.LoadScene(2);
    }

    //HighScore button
    IEnumerator HighScore()
    {
        //Waits so many secounds before starting the next action
        yield return new WaitForSeconds(0.6f);
        //Loads the high score screen
        SceneManager.LoadScene(3);
    }

    //Credit button
    IEnumerator CreditPage()
    {
        //Waits so many secounds before starting the next action
        yield return new WaitForSeconds(0.6f);
        //Loads the high score screen
        SceneManager.LoadScene(4);
    }

    //Quit button
    IEnumerator QuitGame()
    {
        //Waits so many secounds before starting the next action
        yield return new WaitForSeconds(0.6f);
        //Informs button is working
        Debug.Log("Quit!");
        //Quits the game
        Application.Quit();
    }
}

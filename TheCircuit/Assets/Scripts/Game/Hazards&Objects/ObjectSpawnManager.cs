﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectSpawnManager : MonoBehaviour
{
    //***OBJECT SPAWN***
    //This script will focus on the continuous spawn of objects on the road asset within the game.

    //****VARIABLES****
    //Container to assign spawning object 
    public GameObject[] HazardPrefabs;
    //Integer variable (numeric), stores the last prefab as 0
    private int lastPrefabIndex = 0;
    //Float variable (numeric decimal), determind how long the begining camera animation lasts
    private float animationDuration = 4.0f;

    //Creates a countdown game object
    private GameObject CountCanvas;

    //*****UPDATES*****
    //Start is called before the first frame update
    private void Start()
    {
        //If the time is > the animation duration...
        if (Time.timeSinceLevelLoad > animationDuration)
        {
            //Calls the SpawnHazard() function
            SpawnHazard();
        }
    }

    //Update is called once per frame
    private void Update()
    {

    }

    //*****FUNCTIONS*****
    //Spawns objects on tiles
    private void SpawnHazard(int prefabIndex = -1)
    {
        //Names GameObject as go
        GameObject go;
        //Assigns the game object to go
        go = Instantiate(HazardPrefabs[RandomPrefabIndex()]) as GameObject;
        //Choose a random point to spawn the obsticle
        int obsticleSpawnIndex = Random.Range(2, 5);
        //Returns obsticle transform spawn point
        Transform spawnPoint = transform.GetChild(obsticleSpawnIndex).transform;

        //Spawn the obsticle at the position
        Instantiate(go, spawnPoint.position, Quaternion.identity, transform);
    }

    //Spawns random prefabs...
    //..and stopps constant reputition of prefab
    private int RandomPrefabIndex()
    {
        //If there is only one prefab in the list...
        if (HazardPrefabs.Length <= 1)
            //...return the first prefab
            return 0;

        //Integer variable (numeric), stores  last prefab as randomIndex
        int randomIndex = lastPrefabIndex;
        //While the last prefab is the same as the new prefab...
        while(randomIndex == lastPrefabIndex)
        {
            //...randomizes the next prefab
            randomIndex = Random.Range(0, HazardPrefabs.Length);
        }

        //Make lastPrefabIndex equal to the new prefab index
        lastPrefabIndex = randomIndex;
        //Returns new random index
        return randomIndex;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScore : MonoBehaviour
{
    //*****SCORE SCRIPT*****
    //This script controls the score value that the player will accumulate

    //*****VARIABLES*****
    //Integer variable (numeric), stores the end scroe of the player
    public static int endScore;
    //Sets up score as a public string
    Text score;


    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    { 
        //Score isdisplayed as the text
        score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        //Get the score value from Score Script
        endScore = ScoreScript.scoreValue;
        //Text that is displayed onscreen
        score.text = "Score: " + endScore;
    }
}
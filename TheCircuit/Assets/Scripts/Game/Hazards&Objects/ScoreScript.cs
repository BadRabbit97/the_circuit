﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    //*****SCORE SCRIPT*****
    //This script controls the score value that the player will accumulate

    //*****VARIABLES*****
    //Integer variable (numeric), stores the scroe of the player throughout the game
    public static int scoreValue = 0;
    //Sets up score as a public string
    Text score;


    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //Sets the score to 0 at the begining of the scene
        scoreValue = 0;
        //Score isdisplayed as the text
        score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        //Text that is displayed onscreen
        score.text = "Bytes: " + scoreValue;
    }
}

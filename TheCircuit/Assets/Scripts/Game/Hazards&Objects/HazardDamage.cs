﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardDamage : MonoBehaviour
{
    //*****HAZARD DAMAGE*****
    //This script will focus on the damage delt by hazardous objects within the game.

    //****VARIABLES****
    //Integer variable (numeric), stores amount of damage delt
    public int hazardDamage;

    //Stores audio file
    public AudioSource hazardImpact;

    //Makes sure the GameObject has Rigidbody and stores it has rb
    Rigidbody rb;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //*****FUNCTIONS*****
    //On trigger with an object
    void OnTriggerEnter(Collider collisionData)
    {
        // Get the object we collided with
        Collider objectWeCollidedWith = collisionData.GetComponent<Collider>();

        // Get the PlayerHealth script attached to that object (if there is one)
        PlayerHealth player = objectWeCollidedWith.GetComponent<PlayerHealth>();

        // Check if we actually found a player health script
        // This if statement is true if the player variable is NOT null (empty)
        if (player != null)
        {
            // This means there WAS a PlayerHealth script attached to the object we bumped into
            // Which means this object is indeed the player

            // Perform our on-collision action (damage the player)
            player.ChangeHealth(-hazardDamage);

            //Plays audio file
            hazardImpact.Play();
        }
    }
}

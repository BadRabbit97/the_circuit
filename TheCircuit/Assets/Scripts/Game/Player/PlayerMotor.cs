﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMotor : MonoBehaviour
{
    //***CHARACTER MOVEMENT***
    //This script will focus on the characters movement within the game.

    //****VARIABLES****
    //CharacterController variable as controller, allows easy movement constrained by colision without a rigidbody
    private CharacterController controller;
    //A new Vector3 variable, will replace default Vector3 with new moveVector
    private Vector3 moveVector;


    //Float variable (numeric decimal), speed value
    private float speed = 10.0f;
    //Float variable (numeric decimal), determind how long the begining camera animation lasts
    private float animationDuration = 4.0f;
    //Float variable (numeric decimal), how quick the speed increases
    private float speedMultiplier = 1.05f;
    //Float variable (numeric decimal), stores the speed count
    private float speedCount;
    //Float variable (numeric decimal), how far the player needs to travel before speed increase
    private float speedMilestone = 20f;

    //Stores game object, creates a pause menu game object
    public GameObject CountCanvas;

    //*****UPDATES*****
    //Start is called before the first frame update
    void Start()
    {
         //Controller is equal to CharacterController that is assign to the player
        controller = GetComponent<CharacterController> ();

        //Sets spped count to the value of spped milestone
        speedCount = speedMilestone;
    }

    //Update is called once per frame
    void Update()
    {
        //If the time is < the animation duration...
        if(Time.timeSinceLevelLoad < animationDuration)
        {
            //Calls the Countdown function
            Countdown();
            //...player can not be moved
            controller.Move(Vector3.forward * speed * Time.deltaTime);
            //When the animation duration is finished the game will follow it's usual functions
            return;
        }

        //Hides the countdown
        CountCanvas.SetActive(false);

        //If the players position is greater than the speed count...
        if (transform.position.z > speedCount)
        {
            //...the players speed is increased.
            //This increases the speedCount
            speedCount += speedMilestone;

            //This makes sure the speedMilestone increases as the player speeds up
            speedMilestone += speedMilestone * speedMultiplier;

            //This increases the players speed
            speed = speed * speedMultiplier;
        }

        //Recalculate every frame and reset it
        moveVector = Vector3.zero;

        //Recalculate X value, left and right
        moveVector.x = Input.GetAxisRaw("Horizontal") * speed;
        //Recalculate Z value, forward and backwards
        moveVector.z = speed;

        //Moves player forward every single second
        controller.Move(moveVector * Time.deltaTime);

    }

    //*****FUNCTIONS*****
    //Starts the countdown at the begining of the game
    public void Countdown()
    {
        //Displays the countdown onscreen
        CountCanvas.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    //*****PLAYER HEALTH*****
    //This script will focus on the players health within the game.

    //****VARIABLES****
    //Integer variable (numeric), stores starting health for the player
    public int startingHealth;
    //Integer variable (numeric), stores player's current health
    public int currentHealth;

    //Game pause is inactive
    public static bool GameIsPaused = false;

    //Stores game object, creates a HUD game object
    public GameObject HUDMenuUI;

    //Stores game object, creates a pause menu game object
    public GameObject GameOverMenuUI;

    //*****UPDATES*****
    //Start is called before the first frame update
    void Start()
    {
        //When page loads time scale always equals 1
        Time.timeScale = 1f;

        //Stores current health as the starting health at the beginning of the game
        currentHealth = startingHealth;
    }

    //Update is called once per frame
    void Update()
    {

    }

    //*****FUNCTIONS*****
    //Will change the health value of the player
    public void ChangeHealth(int changeAmount)
    {
        //Stores current health as current health plus the changed amount
        currentHealth = currentHealth + changeAmount;

        //Sets parameters for player health, between 0 and the starting health
        currentHealth = Mathf.Clamp(currentHealth, 0, startingHealth);

        //If player health drops to 0, player should die.
        if (currentHealth <= 0)
        {
            //Calling Kill function to kill the player
            Kill();

            //Calling pause function
            Pause();
        }
    }

    //Kills gameObject
    public void Kill()
    {
        //This will destroy the gameObject that this script is attached to
        Destroy(gameObject);
    }

    //This function will allow other scripts to ask what the current health is
    //RETURNS integer, gives number back to the code that called it
    public int GetHealth()
    {
        //Returns the current health value
        return currentHealth;
    }

    //This function will allow other scripts to ask what the max health is
    //RETURNS integer, gives number back to the code that called it
    public int GetMaxHealth()
    {
        //Returns the starting health value
        return startingHealth;
    }

    //Pauses the game
    public void Pause()
    {
        //Shows pause menu when paused
        GameOverMenuUI.SetActive(true);
        //Hides HUD during gameplay
        HUDMenuUI.SetActive(false);
        //Stops/freezes time in the game
        Time.timeScale = 0f;
        //Game pause is active
        GameIsPaused = true;
    }
}

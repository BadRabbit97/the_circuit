﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDisplay : MonoBehaviour
{
    //*****PLAYER HEALTH*****
    //This script will focus on the health display on the HUD within the game.

    //****VARIABLES****
    //GameObject[], contains list of GameObjects
    public GameObject[] healthIcons;

    //Calls variables from the PlayerHealth script
    PlayerHealth player;

    //*****UPDATES*****
    //Start is called before the first frame update
    void Start()
    {
        //Finds and stores the PlayerHealth script variables
        player = FindObjectOfType<PlayerHealth>();
    }

    //Update is called once per frame
    void Update()
    {
        //Create variable to keep track of item on the list that is currently in use
        int currentIconHealth = 0;

        //Loops through all the GameObjects in the list
        //For each step in the loop, it will store the list item in the "icon" variable
        foreach (GameObject icon in healthIcons)
        {
            //Each icon is worth one more health than the last
            //Store currentItemHealth as currentIconHealth plus 1
            currentIconHealth = currentIconHealth + 1;

            //If the player's current health is = or > than  health value for this icon...
            if (player.GetHealth() >= currentIconHealth)
            {
                //...Then turn the icon ON
                icon.SetActive(true);
            }
            //Otherwise (the player's health is LESS than this icon's value)
            else
            {
                //... Turn the icon OFF
                icon.SetActive(false);
            }

        }
    }
}

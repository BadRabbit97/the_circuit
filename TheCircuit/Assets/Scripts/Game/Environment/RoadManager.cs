﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadManager : MonoBehaviour
{
    //***ROAD SPAWN***
    //This script will focus on the continuous spawn of the road asset within the game.

    //****VARIABLES****
    //Container to assign spawning object 
    public GameObject[] roadPrefabs;
    //Position variable playerTransform, holds loction of assigned object
    private Transform playerTransform;
    //Float variable (numeric decimal), where to spawn the object - only position Z as object will always be on the same X & Y co-ordinates!
    private float spawnZ = -4.0f;
    //Float variable (numeric decimal), what length is the object
    private float tileLength = 3.1f;
    //Float variable (numeric decimal), delay for tile deletion
    private float safeZone = 7.0f;
    //Integer variable (numeric), how many tile do we want on the screen
    private int amnTilesOnScreen = 20;

    //Creates a lists for the game object
    private List<GameObject> activeTiles;

    //*****UPDATES*****
    //Start is called before the first frame update
    private void Start()
    {
        //Makes activeTile equal to the new game object
        activeTiles = new List<GameObject>();

        //Assigns "Player" tagged object location to the position variable playerTransform
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        //Start the spawn of the road before the player moves forward
        for (int i = 0; i < amnTilesOnScreen; i++)
        {
            //Calls the SpawnTile() function
            SpawnTile();
        }
    }

    //Update is called once per frame
    private void Update()
    {
        //Continues to spawn the road as the player moves forward
        if (playerTransform.position.z - safeZone > (spawnZ - amnTilesOnScreen * tileLength))
        {
            //Calls the SpawnTile() function
            SpawnTile();
            //Calls the DeleteTile() function
            DeleteTile();
        }
    }

    //*****FUNCTIONS*****
    //Regularly spawns game object
    private void SpawnTile(int prefabIndex = -1)
    {
        //Names GameObject as go
        GameObject go;
        //Assigns the game object to go
        go = Instantiate (roadPrefabs[0]) as GameObject;
        //Spawns the game object at the Tile Manager (parent) location
        go.transform.SetParent(transform);
        //Spawns game object after another
        go.transform.position = Vector3.forward * spawnZ;
        //Makes the next spawn location after the game object length
        spawnZ += tileLength;

        //Adds new object to the lists
        activeTiles.Add(go);
    }

    //Deletes the tiles
    private void  DeleteTile()
    {
        //Destroys game object
        Destroy(activeTiles [0]); 
        //Remove game object from list
        activeTiles.RemoveAt(0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    //***BUILDING SPAWN***
    //This script will focus on the continuous spawn of the skyscraper asset within the game.

    //****VARIABLES****
    //Container to assign spawning object 
    public GameObject[] buildPrefabs;
    //Position variable buildingTransform, holds the loction (transform) of assigned object
    private Transform buildingTransform;
    //Float variable (numeric decimal), where to spawn the object - only position Z as object will always be on the same X & Y co-ordinates!
    private float spawnZ = -4.0f;
    //Float variable (numeric decimal), what length is the object - this will determine the space between objects
    private float tileLength = 18.1f;
    //Float variable (numeric decimal), delay for tile deletion
    private float safeZone = 7.0f;
    //Integer variable (numeric), how many tile do we want on the screen
    private int amnTilesOnScreen = 5;

    //Creates a lists for the game objects
    private List<GameObject> activeTiles;

    //*****UPDATES*****
    //Start is called before the first frame update
    private void Start()
    {
        //Makes activeTile equal to the new game object within the list
        activeTiles = new List<GameObject>();

        //Assigns "Player" tagged object location to the position variable buildingTransform
        //This keeps track of the players movement & allows for the building to spawn point to move along with the player.
        buildingTransform = GameObject.FindGameObjectWithTag("Player").transform;

        //Start the spawn of the skyscraper before the player moves forward
        for (int i = 0; i < amnTilesOnScreen; i++)
        {
            //Calls the SpawnTile() function
            SpawnTile();
        }
    }

    //Update is called once per frame
    private void Update()
    {
        //Continues to spawn the skyscraper as the player moves forward
        if (buildingTransform.position.z - safeZone > (spawnZ - amnTilesOnScreen * tileLength))
        {
            //Calls the SpawnTile() function
            SpawnTile();
            //Calls the DeleteTile() function
            DeleteTile();
        }
        else if(buildingTransform != null)
        {

        }
    }

    //*****FUNCTIONS*****
    //Regularly spawns game object
    private void SpawnTile(int prefabIndex = -1)
    {
        //Names GameObject as go
        GameObject go;
        //Assigns the game object to go
        go = Instantiate(buildPrefabs[0]) as GameObject;
        //Spawns the game object at the Tile Manager (parent) location
        go.transform.SetParent(transform);
        //Spawns game object after another
        Vector3 newPosition = Vector3.forward * spawnZ;
        //Spawns game object at parents x transform position
        newPosition.x = transform.position.x;
        //Spawns game object at parents y transform position
        newPosition.y = transform.position.y;
        //Sets game object to the new position
        go.transform.position = newPosition;
        //Makes the next spawn location after the game object length
        spawnZ += tileLength;

        //Adds new object to the lists
        activeTiles.Add(go);
    }

    //Deletes the tiles
    private void DeleteTile()
    {
        //Destroys game object
        Destroy(activeTiles[0]);
        //Remove game object from list
        activeTiles.RemoveAt(0);
    }
}

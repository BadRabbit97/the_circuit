﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridScroll : MonoBehaviour
{
    //***GRId SCROLL***
    //This script will focus on the grid scrolling movavbility as well as moving with the character within the game.

    //****VARIABLES****
    //Position variable lookAt, holds loction of assigned object
    private Transform GridTransform;
    //Vector3 variable (3D variable), stores cameras location
    private Vector3 startOffSet;
    //A new Vector3 variable, will replace default Vector3 with new moveVector
    private Vector3 moveVector;
    //Float variable (numeric decimal), controls the speed of the scrolling
    public float scrollSpeed = 0.1f;
    //Float variable (numeric decimal), stores the speed of the scrolling
    public float scrollX;

    //Stores the mesh renderer information
    private MeshRenderer meshRender;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //Finds a game object with the tag "Player"
        //Assigns it to the position variable lookAt
        GridTransform = GameObject.FindGameObjectWithTag("Player").transform;
        //Allows the cameras position to be moved within unity
        startOffSet = transform.position - GridTransform.position;

        //Gets the components of the mesh render for the object
        meshRender = GetComponent<MeshRenderer>();
        //Sets the time scale of the scene
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //Positions the cameras location within the game
        moveVector = GridTransform.position + startOffSet;
        //Recalculate X value, left and right
        moveVector.x = 0;

        //Positions the cameras at moveVector
        transform.position = moveVector;

        //Calls the function Scroll()
        Scroll();
    }

    //*****FUNCTIONS*****
    //This function allow the grid floor to scroll through the screen
    private void Scroll()
    {
        //Sets the speed of the scrolling grid
        scrollX = Time.time * scrollSpeed;
        //Sets the speed of the different scroll directions 
        Vector2 offset = new Vector2(0f, scrollX);
        //The texture that should scroll
        meshRender.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}

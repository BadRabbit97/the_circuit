﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMotion : MonoBehaviour
{
    //*****BACKGROUND MOTION*****
    //This script will focus on the moving with the sunset background within the game.

    //****VARIABLES****
    //Position variable lookAt, holds loction of assigned object
    private Transform GridTransform;
    //Vector3 variable (3D variable), stores cameras location
    private Vector3 startOffSet;
    //A new Vector3 variable, will replace default Vector3 with new moveVector
    private Vector3 moveVector;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //Finds a game object with the tag "Player"
        //Assigns it to the position variable lookAt
        GridTransform = GameObject.FindGameObjectWithTag("Player").transform;
        //Allows the cameras position to be moved within unity
        startOffSet = transform.position - GridTransform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Positions the cameras location within the game
        moveVector = GridTransform.position + startOffSet;
        //Recalculate X value, left and right
        moveVector.x = 0;

        //Positions the cameras at moveVector
        transform.position = moveVector;
    }
}

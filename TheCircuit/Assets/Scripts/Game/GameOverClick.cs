﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverClick : MonoBehaviour
{
    //*****GAME OVER CLICK*****
    //This script controls where each button takes the user

    //*****VARIABLES*****
    //Referance MenuBtnControl script, SerializeField = seen but not editable in Editor
    [SerializeField] public MenuBtnControl menuBtnController;

    //
    public AudioSource btnClick;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //When page loads time scale always equals 1
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {

        //If index equal 0 - try again button...
        if (menuBtnController.index == 0)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays Audio file
                btnClick.Play();

                //Call QuitGame function...
                StartCoroutine(TryAgain());
            }
        }

        //If index equal 1 - main menu button...
        if (menuBtnController.index == 1)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays Audio file
                btnClick.Play();

                //Call QuitGame function...
                StartCoroutine(MainMenu());
            }
        }

        //If index equal 2 - quit button...
        if (menuBtnController.index == 2)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays Audio file
                btnClick.Play();

                //Call QuitGame function...
                StartCoroutine(QuitGame());
            }
        }
    }

    //*****FUNCTIONS*****
    //Play button
    IEnumerator TryAgain()
    {
        //Pauses the actions within code before moving onto the next step
        yield return new WaitForSeconds(0.6f);
        //Loads the game level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //MainMenu button
    IEnumerator MainMenu()
    {
        //Pauses the actions within code before moving onto the next step
        yield return new WaitForSeconds(0.6f);
        //Loads the main menu
        SceneManager.LoadScene(1);
    }

    //Quit button
    IEnumerator QuitGame()
    {
        //Pauses the actions within code before moving onto the next step
        yield return new WaitForSeconds(0.6f);
        //Informs button is working
        Debug.Log("Quit!");
        //Quits the game
        Application.Quit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMotor : MonoBehaviour
{
    //*****CAMERA MOVEMENT*****
    //This script will focus on the cameras movement within the game.

    //****VARIABLES****
    //Position variable lookAt, holds loction of assigned object
    private Transform lookAt;
    //Vector3 variable (3D variable), stores cameras location
    private Vector3 startOffSet;
    //A new Vector3 variable, will replace default Vector3 with new moveVector
    private Vector3 moveVector;
    ////A new Vector3 variable, will allow for the camera to start off in a different location at the begining of the game
    private Vector3 animationOffSet = new Vector3(0, 5, -5);

    //Float variable (numeric decimal), transition time
    private float transition = 0.0f;
    //Float variable (numeric decimal), determind how long the begining camera animation lasts
    private float animationDuration = 3.0f;

    //*****UPDATES*****
    //Start is called before the first frame update
    void Start()
    {
        //Finds a game object with the tag "Player"
        //Assigns it to the position variable lookAt
        lookAt = GameObject.FindGameObjectWithTag("Player").transform;
        //Allows the cameras position to be moved within unity
        startOffSet = transform.position - lookAt.position;
    }

    //Update is called once per frame
    void Update()
    {
        //Positions the cameras location within the game
        moveVector = lookAt.position + startOffSet;
        //Recalculate X value, left and right
        moveVector.x = 0;

        //if the transition time > 1 sec...
        if(transition > 1.0f)
        {
            //...positions the cameras at moveVector
            transform.position = moveVector;
        }
        else // if the transition < 1 sec...
        {
            //...the begining animation will be triggered
            //Animation at the start of the game
            transform.position = Vector3.Lerp(moveVector + animationOffSet, moveVector, transition);
            //Animation duration
            transition += Time.deltaTime * 1 / animationDuration;
            //Rotates the camera to look at the player
            transform.LookAt(lookAt.position + Vector3.up);
        }

    }
}

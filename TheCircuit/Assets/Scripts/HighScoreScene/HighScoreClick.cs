﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighScoreClick : MonoBehaviour
{
    //*****HIGHSCORE CLICK*****
    //This script controls where each button takes the user

    //*****VARIABLES*****
    //Referance MenuBtnControl script, SerializeField = seen but not editable in Editor
    [SerializeField] public MenuBtnControl menuBtnController;

    //Stores audio file
    public AudioSource btnClick;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //When page loads time scale always equals 1
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //If index equal 0 - main menu button...
        if (menuBtnController.index == 0)
        {
            //If submit button is pressed...
            if (Input.GetAxis("Submit") == 1)
            {
                //Plays audio file
                btnClick.Play();

                //Call PlayGame function...
                StartCoroutine(MainMenu());
            }
        }
    }

    //MainMenu button
    IEnumerator MainMenu()
    {
        //Code waits untill moving on the next line
        yield return new WaitForSeconds(0.6f);
        //Loads the main menu
        SceneManager.LoadScene(1);
    }
}

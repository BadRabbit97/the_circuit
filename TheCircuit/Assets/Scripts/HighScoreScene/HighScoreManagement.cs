﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreManagement : MonoBehaviour
{
    //*****HIGH SCORE MANAGEMENT*****
    //This script will focus on the code required to create a functioning high scroe table

    //*****VARIABLES*****
    //Position variable entryContainer, holds the location of the entryContainer
    private Transform entryContainer;
    //Position variable entryTemplate, holds the location of the entryTemplate
    private Transform entryTemplate;

    //Creates a list for the high scrores
    private List<Transform> highscoreEnrtyTransformList;

    //*****UPDATES*****
    // Start is called before the first frame update
    private void Awake()
    {
        //entryContainer equals the position of the HighScoreEntryContainer object
        entryContainer = transform.Find("HighScoreEntryContainer");
        //entryTemplate equals the position of the HighScoreEntryTemp object
        entryTemplate = entryContainer.Find("HighScoreEntryTemp");

        //Hides the entryTemplate in the scene start up
        entryTemplate.gameObject.SetActive(false);

        //Adds a new high score entry of the last played level
        AddHighScoreEntry(EndScore.endScore);

        //Load saved scores
        //Stores the list of high scores in the HighScoreTable object
        string jsonString = PlayerPrefs.GetString("HighScoreTable");
        //Returns the high scroe object
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        //Sorts the entry list by score
        //Goes through the entire list...
        for (int i = 0; i < highscores.highscoreEnrtyList.Count; i++)
        {
            //...cycle through all the elements...
            for (int j = i + 1; j < highscores.highscoreEnrtyList.Count; j++)
            {
                //...test to see which has the higher score..
                if (highscores.highscoreEnrtyList[j].score > highscores.highscoreEnrtyList[i].score)
                {
                    //...if needed, swap their positions
                    HighScoreEntry tmp = highscores.highscoreEnrtyList[i];
                    highscores.highscoreEnrtyList[i] = highscores.highscoreEnrtyList[j];
                    highscores.highscoreEnrtyList[j] = tmp;
                }
                //Stops high score going over 10 slots
                //Integer variable (numeric), stores n as 11
                int n = 11;
                //If high score entry list count is greater or equal to n...
                if (highscores.highscoreEnrtyList.Count >= n)
                {
                    //...removes the lowest numbers
                    highscores.highscoreEnrtyList.RemoveAt(n - 1);
                }
            }
            
        }

        //Makes the highscoreEntryList equal to the new list
        highscoreEnrtyTransformList = new List<Transform>();

        //Cycles through the high score list created
        foreach (HighScoreEntry highScoreEntry in highscores.highscoreEnrtyList)
        {
            //Places scores down on the list
            CreateHighScoreEntryTransform(highScoreEntry, entryContainer, highscoreEnrtyTransformList);
        }
    }

    //*****FUNCTIONS*****
    //Adds new trandform entries to the table
    private void CreateHighScoreEntryTransform(HighScoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
        //Defines templates height
        float templateHeight = 30f;

        //Duplicate the highscore entry template
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector3(-24, -templateHeight * transformList.Count, 0);
        //Shows the entryTemplate in the scene
        entryTransform.gameObject.SetActive(true);

        //Integer variable (numeric), stores the ranks position after the first three
        int rank = transformList.Count + 1;
        //String variable (characters), stores the ranks
        string rankString;
        //Creates the ranked hierarchy
        switch (rank)
        {
            default:
                rankString = rank + "th"; break;
            case 1: rankString = "1st"; break;
            case 2: rankString = "2nd"; break;
            case 3: rankString = "3rd"; break;
        }
        //Stores position of rank
        entryTransform.Find("RankText").GetComponent<Text>().text = rankString;

        //Integer variable (numeric), stores the high score entry
        int score = highscoreEntry.score;

        //Stores position of the score
        entryTransform.Find("ScoreText").GetComponent<Text>().text = score.ToString();

        //Stores position of EntryBackground of each odd numbered rank
        entryTransform.Find("EntryBackground").gameObject.SetActive(rank % 2 == 1);

        //If the rank is the first entry...
        if (rank == 1)
        {
            //...change the text to green
            entryTransform.Find("RankText").GetComponent<Text>().color = Color.green;
            entryTransform.Find("ScoreText").GetComponent<Text>().color = Color.green;
        }

        //Adds the transform list
        transformList.Add(entryTransform);
    }

    //Adds the high score entries
    private void AddHighScoreEntry(int score)
    {
        //Creates new high score object and resets the entry list to the HighScoreEntry
        HighScoreEntry highscoreEntry = new HighScoreEntry { score = score };

        //Stores the list of high scores in the HighScoreTable object
        string jsonString = PlayerPrefs.GetString("HighScoreTable");
        //Returns the high scroe object
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        if (highscores == null)
        {
            highscores = new HighScores();
        }

        //Add entries
        highscores.highscoreEnrtyList.Add(highscoreEntry);

        //Save updated scores
        string json = JsonUtility.ToJson(highscores);
        //Stores a string that will store all the high score datas
        PlayerPrefs.SetString("HighScoreTable", json);
        //Saves the PlayerPref
        PlayerPrefs.Save();
    }

    //Creates an object to save the list
    private class HighScores
    {
        //Creates an object to save the list
        public List<HighScoreEntry> highscoreEnrtyList = new List<HighScoreEntry>();
    }

    //*****Represents a single High ecore enrty
    [System.Serializable]
    private class HighScoreEntry
    {
        //Integer variable (numeric), stores the score value
        public int score;
    }

    //Clears the table of entries - no entries on start up of application
    public void clearScoreTable()
    {
        //Load saved scores
        //Stores the list of high scores in the HighScoreTable object
        string jsonString = PlayerPrefs.GetString("HighScoreTable");
        //Returns the high scroe object
        HighScores highscores = JsonUtility.FromJson<HighScores>(jsonString);

        //Clear scores table
        highscores.highscoreEnrtyList.Clear();

        //Save updated scores
        string json = JsonUtility.ToJson(highscores);
        //Stores a string that will store all the high score datas
        PlayerPrefs.SetString("HighScoreTable", json);
        //Saves the PlayerPref
        PlayerPrefs.Save();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTransition : MonoBehaviour
{
    //Awake is called before start, while the scene is being loaded
    private void Awake()
    {
        //Stores game object, stores game object asobjects with the tag GameMusic
        GameObject[] musicObj = GameObject.FindGameObjectsWithTag("GameMusic");

        //If music object is greater than 1...
        if( musicObj.Length > 1)
        {
            //...destroys this object
            Destroy(this.gameObject);
        }

        //When a new scene is loaded does not stop the music
        DontDestroyOnLoad(this.gameObject);
    }
}

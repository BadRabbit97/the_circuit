﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManagment : MonoBehaviour
{
    //*****MUSIC MANAGMENT*****
    //This script will focus on the music that will play throughout the game.

    //****VARIABLES****
    //Stores audio file
    public AudioSource Track1;
    //Stores audio file
    public AudioSource Track2;
    //Stores audio file
    public AudioSource Track3;
    //Stores audio file
    public AudioSource Track4;

    //Integer variable (numeric), stores what track is selected
    public int TrackSelector;
    //Integer variable (numeric), stores the track history
    public int TrackHistory;

    //*****UPDATES*****
    // Start is called before the first frame update
    void Start()
    {
        //Randomly chooses a track
        TrackSelector = Random.Range(0, 3);

        //Plays through the different music
        //If the track selected is 0...
        if(TrackSelector == 0)
        {
            //...play track 1 audio file...
            Track1.Play();
            //...and sets track history to 1
            TrackHistory = 1;
        }
        //If the track selected is 1...
        else if (TrackSelector == 1)
        {
            //...play track 2 audio file...
            Track2.Play();
            //...and sets track history to 2
            TrackHistory = 2;
        }
        //If the track selected is 2...
        else if (TrackSelector == 2)
        {
            //...play track 3 audio file...
            Track3.Play();
            //...and sets track history to 3
            TrackHistory = 3;
        }
        //If the track selected is 3...
        else if (TrackSelector == 3)
        {
            //...play track 4 audio file...
            Track4.Play();
            //...and sets track history to 4
            TrackHistory = 4;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Makes sure music is playing
        //If no music is playing...
        if(Track1.isPlaying == false && Track2.isPlaying == false && Track3.isPlaying == false && Track4.isPlaying == false)
        {
            //Randomly chooses a track
            TrackSelector = Random.Range(0, 3);

            //Plays through the different music
            //Stops the music repeating
            //If the track selected is 0 and was not track 1...
            if (TrackSelector == 0 && TrackHistory != 1)
            {
                //...play track 1 audio file...
                Track1.Play();
                //...and sets track history to 1
                TrackHistory = 1;
            }
            //If the track selected is 1 and was not track 2...
            else if (TrackSelector == 1 && TrackHistory != 2)
            {
                //...play track 2 audio file...
                Track2.Play();
                //...and sets track history to 2
                TrackHistory = 2;
            }
            //If the track selected is 2 and was not track 3...
            else if (TrackSelector == 2 && TrackHistory != 3)
            {
                //...play track 3 audio file...
                Track3.Play();
                //...and sets track history to 3
                TrackHistory = 3;
            }
            //If the track selected is 3 and was not track 4...
            else if (TrackSelector == 3 && TrackHistory != 4)
            {
                //...play track 4 audio file...
                Track4.Play();
                //...and sets track history to 4
                TrackHistory = 4;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartUp : MonoBehaviour
{
    //*****Start Up*****
    //This script will focus on the code required on the start up of the game

    //*****VARIABLES*****
    //Stores the script referance
    public HighScoreManagement refScript;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Calls function in referanced script
        refScript.clearScoreTable();

        //Calls the function ApplicationtStart()
        ApplicationtStart();
    }

    //*****FUNCTIONS*****
    //Play button
    public void ApplicationtStart()
    {
        //Loads the game level
        SceneManager.LoadScene(1);
    }
}
